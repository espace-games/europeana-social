//
//  Clip.h
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A high-level piece of media (a clip, if you will)
//

#ifndef europeanasocial_Clip_h
#define europeanasocial_Clip_h

#import <UIKit/UIKit.h>

@class Video;
@class VideoAsset;

@interface Clip : NSObject {
    NSString* filename;
    int _index;
    
    VideoAsset* _asset;
    Video* _video;
    
    NSString* _title;
}

@property (nonatomic) bool empty;

-(id)initWithFilename:(NSString*)file andVideo:(Video*)vid andIndex:(int)newIndex;
-(UIImage*)thumbnail;
-(VideoAsset*)asset;
-(Video*)video;
-(NSString*)title;
-(int)index;

-(int)lengthSeconds;

@end

#endif

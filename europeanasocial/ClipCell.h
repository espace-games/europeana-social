//
//  ClipCell.h
//  europeanasocial
//
//  Created by Terry Goodwin on 09/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A clip cell, interact by tapping to preview or holding to move
//

#ifndef europeanasocial_ClipCell_h
#define europeanasocial_ClipCell_h

#import <UIKit/UIKit.h>

@class Clip;

@interface ClipCell : UICollectionViewCell {
    IBOutlet UIImageView* thumbnail;
    IBOutlet UILabel* label;
    
    Clip* _clip;
}

@property (nonatomic) bool gotGesture;
@property (nonatomic) IBOutlet UIImageView* header;

-(void)setClip:(Clip*)newClip;
-(IBAction)touchedInfo:(id)sender;

-(UIImage*)getRasterizedImageCopy;
-(Clip*)clip;

@end
    
#endif

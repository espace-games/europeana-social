//
//  Facebook.m
//  europeanasocial
//
//  Created by Terry Goodwin on 16/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Facebook sharing manager
//

#import "Facebook.h"
#import "MediaManager.h"
#import "UIAlertView+SimpleAlert.h"

@implementation Facebook

-(id)initWithMediaManager:(MediaManager*)newManager {
    self = [super init];
    if (self) {
        [self setMediaManager:newManager];
    }
    return self;
}

-(void)setMediaManager:(MediaManager*)newManager {
    mediaManager = newManager;
}

-(void)shareVideoWithCompletionHandler:(FacebookShareCompletion)completionHandler {
    if (sharing) {
        if (completionHandler) {
            completionHandler(false);
        }
        return;
    }
    
    sharing = true;
    
    [mediaManager saveVideoWithCompletionHandler:^(NSURL* savedUrl){
        if (savedUrl == nil) {
            sharing = false;
            if (completionHandler) {
                completionHandler(false);
            }
            return;
        }
        [self openReadSessionWithURL:savedUrl andCompletionHandler:completionHandler];
    }];
}

-(void)shareVideo {
    [self shareVideoWithCompletionHandler:nil];
}

-(void)openReadSessionWithURL:(NSURL*)videoUrl andCompletionHandler:(FacebookShareCompletion)completionHandler {
    [FBSession openActiveSessionWithReadPermissions:@[@"public_profile"] allowLoginUI:YES completionHandler:^(FBSession* newReadSession, FBSessionState status, NSError* readError) {
        if (!newReadSession.isOpen) {
            NSLog(@"Read session is now closed");
            
            if (readSession == newReadSession) {
                readSession = nil;
            }
            return;
        }
        
        NSLog(@"Read session is open");
        
        readSession = newReadSession;
        [readSession close];
        
        [self usePublishPermissionsWithURL:videoUrl andCompletionHandler:completionHandler];
    }];
}

-(void)usePublishPermissionsWithURL:(NSURL*)videoUrl andCompletionHandler:(FacebookShareCompletion)completionHandler {
    [FBSession openActiveSessionWithPublishPermissions:@[@"video_upload"] defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:YES completionHandler:^(FBSession* newSession, FBSessionState status, NSError* publishError) {
        if (!newSession.isOpen) {
            NSLog(@"Existing Publish session is now closed");

            if (newSession == publishSession) {
                publishSession = nil;
            }
            return;
        }
        
        NSLog(@"Existing Publish session is open");
        
        publishSession = newSession;

        if ([publishSession.permissions indexOfObject:@"video_upload"] == NSNotFound) {
            [self requestPublishPermissionsWithURL:videoUrl andCompletionHandler:completionHandler];
        } else {
            [self publishVideoWithURL:videoUrl andCompletionHandler:completionHandler];
        }
    }];
}

-(void)requestPublishPermissionsWithURL:(NSURL*)videoUrl andCompletionHandler:(FacebookShareCompletion)completionHandler {
    [publishSession requestNewPublishPermissions:@[@"video_upload"] defaultAudience:FBSessionDefaultAudienceFriends completionHandler:^(FBSession* newSession, NSError* error) {
        if (!newSession.isOpen) {
            NSLog(@"Requested Publish session is now closed");
            
            requestSession = nil;
            return;
        }
        
        NSLog(@"Requested Publish session is open");
        
        requestSession = newSession;
        
        if (!error) {
            if ([requestSession.permissions indexOfObject:@"video_upload"] == NSNotFound){
                // Permission not granted, tell the user we will not publish
                [UIAlertView simpleAlertWithTitle:@"Permission not granted" andMessage:@"Your VidTime will not be published to Facebook."];
                [requestSession close];
                sharing = false;
                
                if (completionHandler) {
                    completionHandler(false);
                }
            } else {
                NSLog(@"publish_actions permission is granted");
                [self publishVideoWithURL:videoUrl andCompletionHandler:completionHandler];
            }
            
        } else {
            // There was an error, handle it
            // See https://developers.facebook.com/docs/ios/errors/
            [requestSession close];

            if (completionHandler) {
                completionHandler(false);
            }
        }
    }];
}

-(void)publishVideoWithURL:(NSURL*)videoUrl andCompletionHandler:(FacebookShareCompletion)completionHandler {
    
    NSLog(@"publishVideoWithURL %@", videoUrl);
    
    NSError* dataError;
    NSString* filePath = [videoUrl path];
    NSData* videoData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingUncached error:&dataError];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys: videoData, @"video.mp4",
                                   @"My VidTime Dance", @"name",
                                   @"I edited some dance clips together to make this VidTime!", @"description", nil];
    
    [FBRequestConnection startWithGraphPath:@"me/videos" parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection* connection, id result, NSError* requestError) {
        if(!requestError) {
            [UIAlertView simpleAlertWithTitle:@"Video shared" andMessage:@"Your VidTime has been shared to Facebook. Wow!"];

            if (completionHandler) {
                completionHandler(true);
            }
        } else {
            [UIAlertView simpleAlertWithTitle:@"Oh dear" andMessage:requestError.localizedDescription];

            if (completionHandler) {
                completionHandler(false);
            }
        }
        
        if (requestSession != nil) {
            [requestSession close];
        }
        if (publishSession != nil) {
            [publishSession close];
        }
        
        sharing = false;
    }];
}

@end

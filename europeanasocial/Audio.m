//
//  Audio.m
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of audio, instantiated with a filename to create an AudioAsset object
//

#import "Audio.h"

#import "AudioAsset.h"

@implementation Audio

-(id)initWithFilename:(NSString*)file andData:(EuropeanaObject*)europeanaData {
    self = [super initWithFilename:file andData:europeanaData];
    if (self) {
        _asset = [[AudioAsset alloc] initWithFilename:filename];
        
        if (_asset == nil) {
            return nil;
        }
    }
    
    return self;
}

-(int)lengthSeconds {
    CMTime time = [_asset duration];
    return (int)(time.value/time.timescale);
}

-(AudioAsset*)asset {
    return _asset;
}

@end

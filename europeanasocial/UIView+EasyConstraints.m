//
//  UIView+EasyConstraints.m
//  europeanasocial
//
//  Created by Terry Goodwin on 07/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A convenience category to "easily" add constraints to a view programatically
//

#import "UIView+EasyConstraints.h"

@implementation UIView (EasyConstraints)

-(void)constrainInView:(UIView*)parentView {
    [self constrainInView:parentView withConstrantsRect:[self frame]];
}

-(void)constrainInView:(UIView*)parentView withConstrantsRect:(CGRect)constraints; {
    NSDictionary* viewsDictionary = @{@"thisView":self};
    
    NSArray* constraintHeight = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[thisView(%f)]", constraints.size.height]
                                                                        options:0
                                                                        metrics:nil
                                                                          views:viewsDictionary];
    
    NSArray* constraintWidth = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[thisView(%f)]", constraints.size.width]
                                                                       options:0
                                                                       metrics:nil
                                                                         views:viewsDictionary];
    
    NSArray* constraintLeft = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-(%f)-[thisView]", constraints.origin.x]
                                                                      options:0
                                                                      metrics:nil
                                                                        views:viewsDictionary];
    
    NSArray* constraintTop = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-(%f)-[thisView]", constraints.origin.y]
                                                                     options:0
                                                                     metrics:nil
                                                                       views:viewsDictionary];
    
    [self removeConstraintsInView:parentView];
    
    [parentView addConstraints:constraintHeight];
    [parentView addConstraints:constraintWidth];
    [parentView addConstraints:constraintLeft];
    [parentView addConstraints:constraintTop];
}

-(void)removeConstraintsInView:(UIView*)parentView {
    NSMutableArray* constraintsToRemove = [[NSMutableArray alloc] init];
    
    for (NSLayoutConstraint* constraint in parentView.constraints) {
        if (constraint.firstItem == self || constraint.secondItem == self) {
            [constraintsToRemove addObject:constraint];
        }
    }
    
    for (NSLayoutConstraint* constraint in constraintsToRemove) {
        [parentView removeConstraint:constraint];
    }
}

@end

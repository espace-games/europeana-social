//
//  MediaManager.m
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Manager for the media, handles creating video compositions/previews etc.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreMedia/CoreMedia.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "MediaManager.h"

#import "Europeana.h"

#import "Audio.h"
#import "AudioAsset.h"
#import "Clip.h"
#import "PlayerView.h"
#import "Timeline.h"
#import "UIAlertView+SimpleAlert.h"
#import "Video.h"
#import "VideoAsset.h"

@implementation MediaManager

+(NSArray*)VIDEO_IDS {
    return [[NSArray alloc] initWithObjects: @"/2022102/urn_axmedis_00000_obj_945992ca_92b0_4a9a_b0b7_5a2047c5679f",
                                             @"/2022102/urn_axmedis_00000_obj_c9644a14_3c76_4121_a281_c06adf4d9589",
                                             @"/9200123/089E757DFA44F2025C242470D57268854B5A1FE2",
                                             nil];
}
+(NSString*)MUSIC_ID {
    return @"/2023601/oai_eu_dismarc_ISPAN_00000T063506";
}
+(NSString*)BLANK_CLIP_FILENAME {
    return @"blankclip";
}

-(Clip*)blankClip {
    return _blankClip;
}

-(void)setDelegate:(NSObject<MediaManagementDelegate>*)newDelegate {
    _delegate = newDelegate;
    
    if (_music != nil) {
        [_delegate gotMusic:_music];
    }
}

-(UIViewController*)parentViewController {
    return _parentViewController;
}

-(int)numClips {
    return (int)[_clips count];
}
-(NSArray*)clips {
    NSArray* videoClips = [NSArray arrayWithArray:_clips];
    return videoClips;
}

-(Audio*)music {
    return _music;
}

-(void)initialize {
    [self loadData];
}

-(void)getData {
    NSDictionary* options = [[NSDictionary alloc] initWithObjectsAndKeys:[Europeana VALUE_TRUE], [Europeana OPTION_NO_MEDIA], nil];

    NSString* musicId = [MediaManager MUSIC_ID];

    NSMutableArray* mediaIds = [NSMutableArray arrayWithArray:[MediaManager VIDEO_IDS]];
    [mediaIds addObject:musicId];
    
    for (int v = 0; v < [mediaIds count]; v++) {
        NSString* mediaId = [mediaIds objectAtIndex:v];
        
        NSString* mediaType = @"video";
        if (v >= [mediaIds count]-1) {
            mediaType = @"sound";
        }
        
        [Europeana GetRecordWithOptions:options recordId:mediaId type:mediaType request:nil callback:^(EuropeanaObject* mediaRecord) {
            if (mediaRecord != nil) {
                [mediaRecord writeToPList];
            }
        }];
    }
}

-(void)loadData {
    _assets = [[NSMutableArray alloc] init];
    _videos = [[NSMutableArray alloc] init];
    _clips = [[NSMutableArray alloc] init];
    
    _blankClip = [[Clip alloc] initWithFilename:[MediaManager BLANK_CLIP_FILENAME] andVideo:nil andIndex:-1];

    NSArray* videoIds = [MediaManager VIDEO_IDS];
    for (int v = 0; v < [videoIds count]; v++) {
        NSString* videoId = [videoIds objectAtIndex:v];

        EuropeanaObject* europeanaObject = [[EuropeanaObject alloc] initWithPList:videoId];
        [_assets addObject:europeanaObject];
        
        NSString* cleanId = [videoId stringByReplacingOccurrencesOfString:@"/" withString:@""];

        Video* video = [[Video alloc] initWithFilename:cleanId andData:europeanaObject];
        if (video == nil) {
            continue;
        }
        
        [_videos addObject:video];
        
        int index = 1;
        
        while (true) {
            Clip* clip = [[Clip alloc] initWithFilename:[NSString stringWithFormat:@"%@_%d", cleanId, index] andVideo:video andIndex:(int)([_clips count]+1)];
            if (clip == nil) {
                break;
            }
            [_clips addObject:clip];
            
            index++;
        }
    }
    
    NSString* musicId = [MediaManager MUSIC_ID];
    EuropeanaObject* europeanaObject = [[EuropeanaObject alloc] initWithPList:musicId];
    [_assets addObject:europeanaObject];

    NSString* cleanId = [musicId stringByReplacingOccurrencesOfString:@"/" withString:@""];
    _music = [[Audio alloc] initWithFilename:cleanId andData:europeanaObject];
    
    musicItem = [AVPlayerItem playerItemWithAsset:[[_music asset] asset]];
    musicPlayer = [AVPlayer playerWithPlayerItem:musicItem];
    
    if (_music != nil && _delegate != nil) {
        [_delegate gotMusic:_music];
    }
}

-(void)playClip:(Clip*)clip {
    [self hidePlayer];
    
    if (clipPlayer != nil) {
        [clipPlayer pause];
    }
    if ([self playingPreview]) {
        [self pausePreview];
    }

    currentClip = clip;
    
    VideoAsset* video = [clip asset];

    NSString* tracksKey = @"tracks";
    AVAsset* asset = [video asset];
    
    MediaManager* thisManager = self;
    
    [asset loadValuesAsynchronouslyForKeys:@[tracksKey] completionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSError *error;
            AVKeyValueStatus status = [asset statusOfValueForKey:tracksKey error:&error];
            
            if (status == AVKeyValueStatusLoaded) {
                videoItem = [AVPlayerItem playerItemWithAsset:asset];
                
                [videoItem addObserver:self forKeyPath:@"status"
                               options:NSKeyValueObservingOptionInitial context:&ItemStatusContext];
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(playerItemDidReachEnd:)
                                                             name:AVPlayerItemDidPlayToEndTimeNotification
                                                           object:videoItem];
                
                clipPlayer = [AVPlayer playerWithPlayerItem:videoItem];
                [playerView setPlayer:clipPlayer];
                [thisManager showPlayer];
                [clipPlayer play];
            }
            else {
                NSLog(@"The asset's tracks were not loaded:\n%@", [error localizedDescription]);
            }
        });
    }];
}

static const NSString *ItemStatusContext;

-(void)stop {
    [self removePreviewPlayerAndObserver];
}

-(void)removePreviewPlayerAndObserver {
    if (previewPlayer != nil) {
        [previewPlayer removeTimeObserver:playbackObserver];
        [previewPlayer pause];
        previewPlayer = nil;
        
        [playerView setPlayer:nil];
    }
}
-(void)stopClip {
    if (videoItem != nil) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:AVPlayerItemDidPlayToEndTimeNotification
                                                      object:videoItem];
        [videoItem removeObserver:self forKeyPath:@"status" context:&ItemStatusContext];
        videoItem = nil;
    }
    
    if (clipPlayer != nil) {
        [clipPlayer pause];
        clipPlayer = nil;
        
        [playerView setPlayer:nil];
    }
}

-(void)queueClips:(NSArray*)clips {
    [self hidePlayer];

    [self removePreviewPlayerAndObserver];

    previewUrl = nil;
    previewAssets = [[NSMutableArray alloc] init];
    previewComposition = [[AVMutableComposition alloc] init];
    
    AVMutableCompositionTrack* videoCompositionTrack = [previewComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                                       preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack* audioCompositionTrack = [previewComposition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                                       preferredTrackID:kCMPersistentTrackID_Invalid];
    NSError * error = nil;
    
    for (Clip* clip in clips) {
        AVURLAsset* asset = nil;
        
        if (!clip.empty) {
            asset = [[clip asset] asset];
        }
        if (asset == nil) {
            asset = [[_blankClip asset] asset];
        }
        if (asset == nil) {
            continue;
        }
        
        AVAssetTrack* track = [[clip asset] videoTrack];
        if (track == nil) {
            continue;
        }
        
        CMTime duration = previewComposition.duration;
        CMTime insertionPoint = CMTimeMakeWithSeconds((duration.value/duration.timescale)-(1.f/29.97f), duration.timescale);
        
        if ([videoCompositionTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)
                                           ofTrack:track
                                            atTime:insertionPoint
                                             error:&error]) {
            
            [previewAssets addObject:clip];
        } else {
            NSLog(@"Error inserting video into composition: %@",error);
        }
    }
    
    AVURLAsset* musicAsset = [[_music asset] asset];
    AVAssetTrack* musicTrack = [[_music asset] audioTrack];
    
    if (![audioCompositionTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, musicAsset.duration)
                                        ofTrack:musicTrack
                                         atTime:kCMTimeZero
                                          error:&error]) {
        NSLog(@"Error inserting music into composition: %@",error);
    }
    
    previewItem = [[AVPlayerItem alloc] initWithAsset:previewComposition];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        previewPlayer = [AVPlayer playerWithPlayerItem:previewItem];
        [playerView setPlayer:previewPlayer];
    });
}

-(void)setVideoInPreview:(VideoAsset*)video {

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    
    if (context == &ItemStatusContext) {
        return;
    }
    [super observeValueForKeyPath:keyPath ofObject:object
                           change:change context:context];
    return;
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [clipPlayer seekToTime:kCMTimeZero];
    
    if (currentClip != nil) {
        [clipPlayer play];
    }
}

-(float)timelinePreviewOffsetSeconds {
    CMTime timeCode = [timeline timeFromIndicator];
    float timeSeconds = timeCode.value/timeCode.timescale;
    return timeSeconds;
}

-(void)setPreviewOffset {
    if (previewPlayer == nil) {
        return;
    }
    
    [self seekToTimelinePreviewOffset];
}
-(void)setPreviewOffsetWithHighPrecision {
    if (previewPlayer == nil) {
        return;
    }
    
    [self seekToTimelinePreviewOffsetWithHighPrecision];
}

-(void)hidePlayer {
    [playerView setAlpha:0];
}
-(void)showPlayer {
    [playerView setAlpha:1];
}

-(void)seekPreviewPlayerToTime:(CMTime)seekTime withHighPrecision:(bool)highPrecision {
    [self hidePlayer];

    if ([self playingClip]) {
        [self stopClip];
        [playerView setPlayer:previewPlayer];
    }
    
    if (highPrecision) {
        [previewPlayer seekToTime:seekTime completionHandler:^(BOOL finished) {
            if (![self playing]) {
                [self showPlayer];
            }
        }];
    } else {
        [previewPlayer seekToTime:seekTime toleranceBefore:kCMTimeZero toleranceAfter:kCMTimeZero completionHandler:^(BOOL finished) {
            if (![self playing]) {
                [self showPlayer];
            }
        }];
    }
}

-(bool)playing {
    if (![self playingClip] && ![self playingPreview]) {
        return NO;
    }
    return YES;
}
-(bool)playingClip {
    if (clipPlayer == nil) {
        return NO;
    }
    if (videoItem == nil) {
        return NO;
    }
    if (currentClip == nil) {
        return NO;
    }
    return YES;
}
-(bool)playingPreview {
    if (playbackObserver == nil) {
        return NO;
    }
    if (previewItem == nil) {
        return NO;
    }
    if (previewPlayer == nil) {
        return NO;
    }
    if (previewPaused) {
        return NO;
    }
    return YES;
}

-(void)seekToTimelinePreviewOffset {
    float timeSeconds = [self timelinePreviewOffsetSeconds];
    if (timeSeconds >= 0.0) {
        CMTime seekTime = CMTimeMakeWithSeconds(timeSeconds, 1.f);
        [self seekPreviewPlayerToTime:seekTime withHighPrecision:NO];
    }
}
-(void)seekToTimelinePreviewOffsetWithHighPrecision {
    float timeSeconds = [self timelinePreviewOffsetSeconds];
    if (timeSeconds >= 0.0) {
        CMTime seekTime = CMTimeMakeWithSeconds(timeSeconds, 1.f);
        [self seekPreviewPlayerToTime:seekTime withHighPrecision:YES];
    }
}

-(void)playPreview {
    if ([self playingPreview]) {
        return;
    }
    [self hidePlayer];
    
    if ([self playingClip]) {
        [self stopClip];

        [playerView setPlayer:previewPlayer];
    }
    
    previewPaused = false;
    
    [self seekToTimelinePreviewOffsetWithHighPrecision];
    [self playPreviewAndSetupObserver];
    
    [self showPlayer];
}
-(void)pausePreview {
    if (![self playingPreview]) {
        return;
    }
    
    [previewPlayer pause];
    previewPaused = true;
}

-(void)playPreviewAndSetupObserver {
    if (playbackObserver != nil) {
        [previewPlayer removeTimeObserver:playbackObserver];
        playbackObserver = nil;
    }
    
    [previewPlayer play];
    
    Timeline* ourTimeline = timeline;
    
    CMTime interval = CMTimeMake(15, 1000);
    playbackObserver = [previewPlayer addPeriodicTimeObserverForInterval:interval queue:NULL usingBlock:^(CMTime currentTime) {
        float seconds = CMTimeGetSeconds(currentTime);
        [ourTimeline moveIndicatorToTimeWithSeconds:seconds];
    }];
}

-(bool)canSaveVideo {
    if (previewComposition == nil) {
        return NO;
    }
    if (previewAssets == nil) {
        return NO;
    }
    if ([previewAssets count] <= 0) {
        return NO;
    }
    
    for (Clip* clip in previewAssets) {
        if (clip != _blankClip) {
            return YES;
        }
    }
    return NO;
}

-(void)saveVideoWithCompletionHandler:(SavedVideoCallback)completionHandler {
    if (previewUrl != nil) {
        if (completionHandler != nil) {
            completionHandler(previewUrl);
        }
        return;
    }
    
    if (![self canSaveVideo]) {
        [UIAlertView simpleAlertWithTitle:@"Cannot share" andMessage:@"Drag and drop some clips to the timeline!"];
        if (completionHandler != nil) {
            completionHandler(nil);
        }
        return;
    }
    
    static NSDateFormatter *kDateFormatter;
    if (!kDateFormatter) {
        kDateFormatter = [[NSDateFormatter alloc] init];
        kDateFormatter.dateStyle = NSDateFormatterMediumStyle;
        kDateFormatter.timeStyle = NSDateFormatterShortStyle;
    }

    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:previewComposition presetName:AVAssetExportPresetMediumQuality];

    exporter.outputURL = [[[NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES] URLByAppendingPathComponent:[kDateFormatter stringFromDate:[NSDate date]]] URLByAppendingPathExtension:CFBridgingRelease(UTTypeCopyPreferredTagWithClass((CFStringRef)AVFileTypeMPEG4, kUTTagClassFilenameExtension))];

    exporter.outputFileType = AVFileTypeMPEG4;
    exporter.shouldOptimizeForNetworkUse = YES;

    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (exporter.status == AVAssetExportSessionStatusCompleted) {
                previewUrl = exporter.outputURL;
                if (completionHandler != nil) {
                    completionHandler(previewUrl);
                }
            }
        });
    }];
}

@end

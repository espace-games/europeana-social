//
//  PlayerView.m
//  europeanasocial
//
//  Created by Terry Goodwin on 09/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A view containing the video preview
//

#import "PlayerView.h"

#import <AVFoundation/AVFoundation.h>

@implementation PlayerView

+(Class)layerClass {
    return [AVPlayerLayer class];
}
-(AVPlayer*)player {
    return [(AVPlayerLayer *)[self layer] player];
}
-(void)setPlayer:(AVPlayer *)player {
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}

@end

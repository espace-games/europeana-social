//
//  NSString+Additions.h
//  europeanasocial
//
//  Created by Terry Goodwin on 12/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A convenience category to add some simple functionality to NSString
//

#ifndef europeanasocial_NSString_Additions_h
#define europeanasocial_NSString_Additions_h

#import <Foundation/Foundation.h>

@interface NSString (Additions)

+(NSString*)stringFromInt:(NSNumber*)number padZeroes:(bool)padding;
+(BOOL)stringIsValid:(NSString*)string;

@end

#endif
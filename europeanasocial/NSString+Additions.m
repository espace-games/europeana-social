//
//  NSString+Additions.m
//  europeanasocial
//
//  Created by Terry Goodwin on 12/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A convenience category to add some simple functionality to NSString
//

#import "NSString+Additions.h"

@implementation NSString (Additions)

+(NSString*)stringFromInt:(NSNumber*)number padZeroes:(bool)padding {
    NSMutableString* string = [NSMutableString stringWithString:@""];
    if (padding && [number intValue] < 10) {
        string = [NSMutableString stringWithString:@"0"];
    }
    [string appendString:[NSString stringWithFormat:@"%@", number]];
    
    return [NSString stringWithString:string];
}

+(BOOL)stringIsValid:(NSString*)string {
    if (string == nil) {
        return NO;
    }
    if ([string isEqual: [NSNull null]]) {
        return NO;
    }
    if (string.length <= 0) {
        return NO;
    }
    return YES;
}

@end

//
//  TimelineClip.m
//  europeanasocial
//
//  Created by Terry Goodwin on 07/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of media on the timeline with a label
//

#import "TimelineMedia.h"

@implementation TimelineMedia

@end

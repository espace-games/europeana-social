//
//  Timeline.m
//  europeanasocial
//
//  Created by Terry Goodwin on 07/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Handles the placement of clips and moving around on the timeline
//

#import "Timeline.h"

#import "Audio.h"
#import "Clip.h"
#import "MediaManager.h"
#import "TimelineAudio.h"
#import "TimelineVideo.h"
#import "VideoAsset.h"

#import "NSString+Additions.h"
#import "UIView+EasyConstraints.h"

@implementation Timeline

+(float)INCREMENT_LENGTH_SECONDS {
    return 5.f;
}
+(int)SLOT_WIDTH {
    return 85;
}
+(int)SLOT_MARGIN {
    return 5;
}
+(int)SLOT_ITEM_OFFSET {
    return 1;
}
+(int)LABEL_WIDTH {
    return 50;
}
+(int)LABEL_HEIGHT {
    return 20;
}
+(float)PREVIEW_UPDATE_INTERVAL_SECONDS {
    return 1.0;
}

-(void)setScrollView:(UIScrollView*)newView {
    scrollView = newView;

    double delayInSeconds = 0.2f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self initIndicator];
    });
}

-(void)initIndicator {
    indicator = (UIView*)[[[NSBundle mainBundle] loadNibNamed:@"TimelineIndicator" owner:self options:nil] objectAtIndex:0];

    [scrollView addSubview:indicator];
    [indicator removeConstraintsInView:scrollView];
    
    CGRect frame = [indicator frame];
    frame.size.width = 18;
    frame.size.height = [scrollView frame].size.height;
    frame.origin.y = 0;
    frame.origin.x = [Timeline SLOT_MARGIN]+([Timeline SLOT_WIDTH]*1.5)-(frame.size.width/2);
    
    [indicator setFrame:frame];

    UIPanGestureRecognizer* dragGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragIndicator:)];
    [indicator addGestureRecognizer:dragGesture];
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTimeline:)];
    [scrollView addGestureRecognizer:tapGesture];
}

-(void)dragIndicator:(UIPanGestureRecognizer*)sender {
    if ([mediaManager playingPreview]) {
        return;
    }
    
    CGPoint loc = [sender locationInView:scrollView];

    if (sender.state == UIGestureRecognizerStateBegan) {
        draggingIndicator = true;
    } else if (sender.state == UIGestureRecognizerStateChanged) {
        if (!draggingIndicator) {
            return;
        }
        
        [self moveIndicatorToPoint:loc];
        [self updatePreviewPositionIfCan];
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        draggingIndicator = false;
        [self updatePreviewPosition];
    }
}

-(void)tapTimeline:(UITapGestureRecognizer*)sender {
    CGPoint loc = [sender locationInView:scrollView];
    if (loc.y > [Timeline LABEL_HEIGHT]) {
        return;
    }

    [self moveIndicatorToPoint:loc];
    [self updatePreviewPositionWithHighPrecision];
}

-(void)moveIndicatorToTimeWithSeconds:(float)seconds {
    float point = ((float)[Timeline SLOT_WIDTH]/(float)[Timeline INCREMENT_LENGTH_SECONDS])*seconds;
    
    CGPoint loc = CGPointMake(point, 0);
    [self moveIndicatorToPoint:loc];
}

-(void)moveIndicatorToPoint:(CGPoint)loc {
    loc.y = [scrollView frame].size.height/2;

    CGRect frame = [indicator frame];
    frame.origin.x = loc.x-(frame.size.width/2);
    [indicator setFrame:frame];
    
    NSLog(@"moveIndicatorToPoint %f", loc.x);
}

-(void)skipPreviewForwards {
    CMTime currentTime = [self timeFromIndicator];
    float currentTimeSeconds = currentTime.value/currentTime.timescale;
    if (currentTimeSeconds >= [self length]) {
        return;
    }
    
    float segmentLength = [Timeline INCREMENT_LENGTH_SECONDS];
    float newTimeSeconds = (((int)currentTimeSeconds/(int)segmentLength)+1)*segmentLength;
    
    if (newTimeSeconds > [self length]) {
        newTimeSeconds = [self length];
    }
    
    [self moveIndicatorToTimeWithSeconds:newTimeSeconds];
}
-(void)skipPreviewBackwards {
    CMTime currentTime = [self timeFromIndicator];
    float currentTimeSeconds = currentTime.value/currentTime.timescale;
    if (currentTimeSeconds <= 0.f) {
        return;
    }
    
    float segmentLength = [Timeline INCREMENT_LENGTH_SECONDS];
    float newTimeSeconds = (((int)currentTimeSeconds/(int)segmentLength)-1)*segmentLength;
    
    if (newTimeSeconds < 0.f) {
        newTimeSeconds = 0.f;
    }
    
    [self moveIndicatorToTimeWithSeconds:newTimeSeconds];
}
-(void)rewind {
    CMTime currentTime = [self timeFromIndicator];
    float currentTimeSeconds = currentTime.value/currentTime.timescale;
    if (currentTimeSeconds <= 0.f) {
        return;
    }
    
    float newTimeSeconds = currentTimeSeconds-1.f;
    if (newTimeSeconds < 0.f) {
        newTimeSeconds = 0.f;
    }
    
    [self moveIndicatorToTimeWithSeconds:newTimeSeconds];
}
-(void)fastForward {
    CMTime currentTime = [self timeFromIndicator];
    float currentTimeSeconds = currentTime.value/currentTime.timescale;
    if (currentTimeSeconds >= [self length]) {
        return;
    }
    
    float newTimeSeconds = currentTimeSeconds+1.f;
    if (newTimeSeconds > [self length]) {
        newTimeSeconds = [self length];
    }
    
    [self moveIndicatorToTimeWithSeconds:newTimeSeconds];
}

-(CMTime)timeFromIndicator {
    CGRect frame = [indicator frame];
    int position = frame.origin.x+(frame.size.width/2);
    float time = ((float)position/(float)[Timeline SLOT_WIDTH])*[Timeline INCREMENT_LENGTH_SECONDS];
    NSLog(@"timeFromIndicator %f", time);
    return CMTimeMakeWithSeconds(time, clipTimescales);
}

-(void)updatePreviewPositionIfCan {
    if (lastPreviewUpdateTime == nil) {
        [self updatePreviewPosition];
        return;
    }
    
    NSDate* currentTime = [NSDate date];
    NSTimeInterval diff = [currentTime timeIntervalSinceDate:lastPreviewUpdateTime];
    
    if (diff >= [Timeline PREVIEW_UPDATE_INTERVAL_SECONDS]) {
        [self updatePreviewPosition];
    }
}
-(void)updatePreviewPositionWithHighPrecision {
    lastPreviewUpdateTime = [NSDate date];
    
    NSLog(@"Updating %@", lastPreviewUpdateTime);
    
    if (mediaManager == nil) {
        return;
    }
    [mediaManager setPreviewOffsetWithHighPrecision];
}
-(void)updatePreviewPosition {
    lastPreviewUpdateTime = [NSDate date];
    
    NSLog(@"Updating %@", lastPreviewUpdateTime);

    if (mediaManager == nil) {
        return;
    }
    [mediaManager setPreviewOffset];
}

-(bool)droppedClip:(Clip*)clip withGestureRecognizer:(UILongPressGestureRecognizer*)gesture {
    if ([mediaManager playingPreview]) {
        return false;
    }
    
    CGPoint location = [gesture locationInView:scrollView];
    if (location.y < 0) {
        return false;
    }
    
    TimelineVideo* newVideoView = (TimelineVideo*)[[[NSBundle mainBundle] loadNibNamed:@"TimelineVideo" owner:self options:nil] objectAtIndex:0];
    [newVideoView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [newVideoView setClip:clip];

    int positionX = location.x/[Timeline SLOT_WIDTH];
    
    NSNumber* slotIndex = [NSNumber numberWithInt:positionX];
    
    TimelineVideo* current = nil;
    
    @try {
        current = [slots objectForKey:slotIndex];
    } @catch (NSException* exception) {
        current = nil;
    }
    
    if (current != nil) {
        [current removeFromSuperview];
    }
    
    [slots setObject:newVideoView forKey:slotIndex];
    
    positionX *= [Timeline SLOT_WIDTH];
    
    CGRect videoFrame = [newVideoView frame];
    videoFrame.origin.x = positionX+([Timeline SLOT_MARGIN]/2)+[Timeline SLOT_ITEM_OFFSET];
    videoFrame.origin.y = 30;
    videoFrame.size.width = [self widthFromDuration:[clip lengthSeconds]]-[Timeline SLOT_MARGIN];
    
    [newVideoView setFrame:videoFrame];
    
    [scrollView addSubview:newVideoView];
    [newVideoView constrainInView:scrollView];

    [self bringIndicatorToFront];
    return true;
}

-(void)removeClip:(TimelineVideo*)clip {
    for (int i = 0; i < maxSlots; i++) {
        NSNumber* slotIndex = [NSNumber numberWithInt:i];
        TimelineVideo* other = [slots objectForKey:slotIndex];
        
        if (other == clip) {
            [other removeFromSuperview];
            
            TimelineVideo* video = [[TimelineVideo alloc] init];
            [video setClip:blankClip];
            [slots setObject:video forKey:slotIndex];
            
            NSArray* newClips = [self clips];
            [mediaManager queueClips:newClips];
            return;
        }
    }
}

-(void)bringIndicatorToFront {
    [scrollView bringSubviewToFront:indicator];
}

-(float)length {
    return (float)(maxSlots*[Timeline INCREMENT_LENGTH_SECONDS]);
}

-(void)initializeWithMusic:(Audio*)music {
    if (initialized) {
        return;
    }

    if (audio != nil) {
        [audio removeFromSuperview];
        audio = nil;
    }
    
    TimelineAudio* newAudioView = (TimelineAudio*)[[[NSBundle mainBundle] loadNibNamed:@"TimelineAudio" owner:self options:nil] objectAtIndex:0];
    [newAudioView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [newAudioView setAudio:music];

    int length = [music lengthSeconds];
    int width = [self widthFromDuration:length];
    
    maxSlots = length/(int)[Timeline INCREMENT_LENGTH_SECONDS];

    [self initializeSlots];

    CGRect audioFrame = [newAudioView frame];
    audioFrame.origin.x = [Timeline SLOT_MARGIN]/2;
    audioFrame.origin.y = 110;
    audioFrame.size.width = width-[Timeline SLOT_MARGIN];
    
    [newAudioView setFrame:audioFrame];
    
    audio = newAudioView;
    
    scrollView.contentSize = CGSizeMake(width, scrollView.frame.size.height);
    
    float current = 0;
    int index = 0;
    
    while (current <= length) {
        UIView* splitter = [[UIView alloc] init];
        [splitter.layer setBorderWidth:1.0];
        [splitter.layer setBorderColor:[[UIColor colorWithPatternImage:[UIImage imageNamed:@"divider.png"]] CGColor]];
        
        CGRect splitterFrame = [splitter frame];
        splitterFrame.origin.x = (index*[Timeline SLOT_WIDTH]);
        splitterFrame.origin.y = 20;
        splitterFrame.size.width = 1;
        splitterFrame.size.height = scrollView.frame.size.height-10;
        
        [splitter setFrame:splitterFrame];
        [scrollView addSubview:splitter];
        [splitter constrainInView:scrollView];

        float minutesFloating = current/60.f;
        NSNumber* minutes = [NSNumber numberWithInt:(int)minutesFloating];
        
        float secondsFloating = (minutesFloating-(float)[minutes intValue])*60.f;
        NSNumber* seconds = [NSNumber numberWithInt:(int)secondsFloating];
        
        UILabel* timeLabel = [[UILabel alloc] init];
        timeLabel.text = [NSString stringWithFormat:@"%@:%@", [NSString stringFromInt:minutes padZeroes:TRUE], [NSString stringFromInt:seconds padZeroes:TRUE]];
        timeLabel.textColor = scrollView.tintColor;
        timeLabel.font = [timeLabel.font fontWithSize:12];
        timeLabel.textAlignment = NSTextAlignmentCenter;
        
        CGRect labelFrame = [timeLabel frame];
        labelFrame.origin.x = (index*[Timeline SLOT_WIDTH])-([Timeline LABEL_WIDTH]/2)+[Timeline SLOT_ITEM_OFFSET];
        labelFrame.origin.y = 0;
        labelFrame.size.width = [Timeline LABEL_WIDTH];
        labelFrame.size.height = [Timeline LABEL_HEIGHT];
        
        [timeLabel setFrame:labelFrame];
        [scrollView addSubview:timeLabel];
        [timeLabel constrainInView:scrollView];
        
        index++;
        current += [Timeline INCREMENT_LENGTH_SECONDS];
    }
    
    [scrollView addSubview:newAudioView];
    [newAudioView constrainInView:scrollView];

    [self bringIndicatorToFront];
    
    initialized = true;
}

-(int)widthFromDuration:(int)durationSeconds {
    int width = (int)((float)durationSeconds/[Timeline INCREMENT_LENGTH_SECONDS])*(float)[Timeline SLOT_WIDTH];
    return width;
}

-(NSArray*)clips {
    NSMutableArray* allClips = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < maxSlots; i++) {
        NSNumber* slotIndex = [NSNumber numberWithInt:i];
        TimelineVideo* video = [slots objectForKey:slotIndex];
        Clip* clip = nil;
        
        if (video != nil) {
            clip = [video clip];
        }

        if (clip == nil) {
            clip = [[Clip alloc] init];
        }
        [allClips addObject:clip];
    }
    
    return [[NSArray alloc] initWithArray:allClips];
}

-(void)initializeSlots {
    slots = [[NSMutableDictionary alloc] init];
    
    if (blankClip == nil) {
        blankClip = [mediaManager blankClip];
        clipTimescales = [[blankClip asset] asset].duration.timescale;
    }
    
    for (int i = 0; i < maxSlots; i++) {
        NSNumber* slotIndex = [NSNumber numberWithInt:i];
        TimelineVideo* video = [[TimelineVideo alloc] init];
        [video setClip:blankClip];
        [slots setObject:video forKey:slotIndex];
    }
    
    NSArray* blankClips = [self clips];
    [mediaManager queueClips:blankClips];
}

@end




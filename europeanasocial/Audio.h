//
//  Audio.h
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of audio, instantiated with a filename to create an AudioAsset object
//

#ifndef europeanasocial_Audio_h
#define europeanasocial_Audio_h

#import "Media.h"

@class AudioAsset;

@interface Audio : Media {
    AudioAsset* _asset;
}

-(AudioAsset*)asset;

@end

#endif

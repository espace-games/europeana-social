//
//  MediaAsset.h
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A low-level audio-visual media asset
//

#ifndef europeanasocial_MediaAsset_h
#define europeanasocial_MediaAsset_h

#import <AVFoundation/AVFoundation.h>

@interface MediaAsset : NSObject {
    AVURLAsset* _asset;
    NSURL* url;
    NSString* filename;
}

-(NSString*)extension;
-(id)initWithFilename:(NSString*)file;
-(AVURLAsset*)asset;
-(AVAssetTrack*)audioTrack;
-(AVAssetTrack*)videoTrack;
-(CMTime)duration;

@end

#endif

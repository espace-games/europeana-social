//
//  TimelineAudio.m
//  europeanasocial
//
//  Created by Terry Goodwin on 07/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of audio on the timeline, inherits a label from TimelineMedia
//

#import "TimelineAudio.h"

#import "Audio.h"
#import "Europeana.h"

@implementation TimelineAudio

-(void)setAudio:(Audio*)newAudio {
    audio = newAudio;
    
    EuropeanaObject* data = [audio data];

    label.text = [NSString stringWithFormat:@"Music - %@ - %@", [data title], [data attribution]];
}

@end

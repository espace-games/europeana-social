//
//  ClipsCollectionViewController.m
//  europeanasocial
//
//  Created by Terry Goodwin on 09/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Controller for the scrollable collection of clips
//

#import "ClipsCollectionViewController.h"

#import "AppDelegate.h"
#import "Clip.h"
#import "ClipCell.h"
#import "MediaManager.h"
#import "MainController.h"

@implementation ClipsCollectionViewController

-(void)viewWillAppear:(BOOL)animated {
    [self.collectionView reloadData];
    
    currentIndex = -1;
}

-(NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section {
    [self addClips];
    
    return [mediaManager numClips];
}

-(void)dragCell:(UILongPressGestureRecognizer*)sender {
    MainController* topViewController = (MainController*)[mediaManager parentViewController];
    
    CGPoint loc = [sender locationInView:self.collectionView];
    if (sender.state == UIGestureRecognizerStateBegan) {
        dragStartIndex = [self.collectionView indexPathForItemAtPoint:loc];
        
        if (dragStartIndex) {
            ClipCell* cell = (ClipCell*)[self.collectionView cellForItemAtIndexPath:dragStartIndex];
            draggingView = [[UIImageView alloc] initWithImage:[cell getRasterizedImageCopy]];
            CGAffineTransform transform = CGAffineTransformMakeScale(1.1f, 1.1f);
            draggingView.transform = transform;
            
            [cell setAlpha:0.0f];
            [topViewController.view addSubview:draggingView];
            
            loc = [sender locationInView:topViewController.view];
            draggingView.center = loc;
            
            dragViewStartLocation = draggingView.center;
            [topViewController.view bringSubviewToFront:draggingView];
        }
    } else if (sender.state == UIGestureRecognizerStateChanged) {
        loc = [sender locationInView:topViewController.view];
        draggingView.center = loc;
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        if (dragStartIndex) {
            ClipCell* cell = (ClipCell*)[self.collectionView cellForItemAtIndexPath:dragStartIndex];
            [cell setAlpha:1.0f];

            [topViewController droppedClip:[cell clip] withGestureRecognizer:sender];
        }
        
        if (draggingView) {
            
            [UIView animateWithDuration:.1f animations:^{
                draggingView.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                [draggingView removeFromSuperview];
                draggingView = nil;
            }];
            dragStartIndex = nil;
            
            loc = CGPointZero;
        }
    }
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath*)indexPath {
    ClipCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"clipCell" forIndexPath:indexPath];

    if (!cell.gotGesture) {
        cell.gotGesture = true;
        UILongPressGestureRecognizer* dragGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(dragCell:)];
        [cell addGestureRecognizer:dragGesture];
    }
    
    NSUInteger length = [indexPath length];
    NSUInteger theIndex = [indexPath indexAtPosition:length-1];

    if (theIndex == currentIndex) {
        [self selectCell:cell withIndex:theIndex];
    } else {
        [self deselectCell:cell];
    }
    
    Clip* clip = [[mediaManager clips] objectAtIndex:theIndex];
    [cell setClip:clip];

    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath*)indexPath {
    ClipCell* cell = (ClipCell*)[self.collectionView cellForItemAtIndexPath:indexPath];

    NSUInteger length = [indexPath length];
    NSUInteger theIndex = [indexPath indexAtPosition:length-1];

    if (currentIndex == theIndex) {
        return;
    }
    
    [self selectCell:cell withIndex:theIndex];
    
    Clip* clip = [[mediaManager clips] objectAtIndex:theIndex];
    [mediaManager playClip:clip];
}

-(void)addClips {
    if (mediaManager != nil) {
        return;
    }
    
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (appDelegate == nil) {
        return;
    }
    
    mediaManager = [appDelegate mediaManager];
}

-(void)selectCell:(ClipCell*)cell withIndex:(NSUInteger)index {
    if (currentIndex != -1 && currentIndex != index) {
        currentIndex = index;
        [self.collectionView reloadData];
        return;
    }

    UIColor* selectColor = self.collectionView.tintColor;
    cell.backgroundColor = selectColor;
    currentIndex = index;
}

-(void)deselectCell:(ClipCell*)cell {
    cell.backgroundColor = [UIColor clearColor];
}


@end

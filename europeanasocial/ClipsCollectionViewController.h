//
//  ClipsCollectionViewController.h
//  europeanasocial
//
//  Created by Terry Goodwin on 09/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Controller for the scrollable collection of clips
//

#ifndef europeanasocial_ClipsCollectionViewController_h
#define europeanasocial_ClipsCollectionViewController_h

#import <UIKit/UIKit.h>

@class MediaManager;
@class ClipCell;

@interface ClipsCollectionViewController : UICollectionViewController {
    MediaManager* mediaManager;
    
    NSUInteger currentIndex;

    UIImageView* draggingView;
    NSIndexPath* dragStartIndex;
    CGPoint dragViewStartLocation;
}

@end

#endif

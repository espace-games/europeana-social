//
//  TimelineAudio.h
//  europeanasocial
//
//  Created by Terry Goodwin on 07/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of audio on the timeline, inherits a label from TimelineMedia
//

#ifndef europeanasocial_TimelineAudio_h
#define europeanasocial_TimelineAudio_h

#import "TimelineMedia.h"

@class Audio;

@interface TimelineAudio : TimelineMedia {
    Audio* audio;
}

-(void)setAudio:(Audio*)newAudio;

@end

#endif

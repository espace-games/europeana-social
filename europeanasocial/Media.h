//
//  Media.h
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of media - a Europeana data object coupled with a corresponding file
//

#ifndef europeanasocial_Media_h
#define europeanasocial_Media_h

#import <Foundation/Foundation.h>

@class EuropeanaObject;

@interface Media : NSObject {
    EuropeanaObject* _data;
    NSString* filename;
}

-(id)initWithFilename:(NSString*)file andData:(EuropeanaObject*)europeanaData;
-(EuropeanaObject*)data;
-(int)lengthSeconds;

@end

#endif

//
//  main.m
//  europeanasocial
//
//  Created by SGInt_003 on 06/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

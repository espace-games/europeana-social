//
//  TimelineVideo.m
//  europeanasocial
//
//  Created by Terry Goodwin on 07/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of video on the timeline, inherits a label from TimelineMedia
//

#import "TimelineVideo.h"

#import "Clip.h"
#import "MainController.h"

@implementation TimelineVideo

-(void)setClip:(Clip*)newClip {
    _clip = newClip;

    label.text = [_clip title];
    thumbnail.image = [_clip thumbnail];
}

-(Clip*)clip {
    return _clip;
}

-(IBAction)removeClip:(id)sender {
    MainController* mainController = (MainController*)self.window.rootViewController;
    [mainController removeClipFromTimeline:self];
}

@end

//
//  Playback.m
//  europeanasocial
//
//  Created by SGInt_003 on 15/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Controller for the playback buttons
//

#import "Playback.h"

#import "MediaManager.h"
#import "Timeline.h"

@implementation Playback

+(int)FAST_REPEAT_DELAY_SECONDS {
    return 0.25f;
}

-(IBAction)touchedBack:(id)sender {
    [timeline skipPreviewBackwards];
}
-(IBAction)touchedRewind:(id)sender {
    if ([mediaManager playingPreview]) {
        [self touchedPlayPause:sender];
    }

    doRW = true;
    [self doRewind];
}
-(IBAction)unTouchedRewind:(id)sender {
    doRW = false;
}
-(IBAction)cancelledRewind:(id)sender {
    [self unTouchedRewind:sender];
}
-(IBAction)unCancelledRewind:(id)sender {
    [self touchedRewind:sender];
}
-(IBAction)touchedPlayPause:(id)sender {
    if ([mediaManager playingPreview]) {
        [mediaManager pausePreview];
        [self setPlayButton];
        return;
    }
    
    [mediaManager playPreview];
    [self setPauseButton];
    return;
}
-(IBAction)touchedFastForward:(id)sender {
    if ([mediaManager playingPreview]) {
        [self touchedPlayPause:sender];
    }

    doFF = true;
    [self doFastForward];
}
-(IBAction)unTouchedFastForward:(id)sender {
    doFF = false;
}
-(IBAction)cancelledFastForward:(id)sender {
    [self unTouchedFastForward:sender];
}
-(IBAction)unCancelledFastForward:(id)sender {
    [self touchedFastForward:sender];
}
-(IBAction)touchedForward:(id)sender {
    [timeline skipPreviewForwards];
}

-(void)setPlayButton {
    [self setButtonImages];
    
    [playPause setImage:playImage forState:UIControlStateNormal];
}
-(void)setPauseButton {
    [self setButtonImages];
    
    [playPause setImage:pauseImage forState:UIControlStateNormal];
}

-(void)setButtonImages {
    if (playImage == nil) {
        playImage = playPause.imageView.image;
    }
    if (pauseImage == nil) {
        pauseImage = [UIImage imageNamed:@"button_pause.png"];
    }
}

-(void)doRewind {
    if (!doRW) {
        return;
    }
    
    [timeline rewind];
    [self performSelector:@selector(doRewind) withObject:nil afterDelay:[Playback FAST_REPEAT_DELAY_SECONDS]];
}
-(void)doFastForward {
    if (!doFF) {
        return;
    }

    [timeline fastForward];
    [self performSelector:@selector(doFastForward) withObject:nil afterDelay:[Playback FAST_REPEAT_DELAY_SECONDS]];
}

@end

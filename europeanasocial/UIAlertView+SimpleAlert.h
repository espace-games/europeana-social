//
//  UIAlertView+SimpleAlert.h
//  europeanasocial
//
//  Created by Terry Goodwin on 16/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A convenience category to easily and cleanly show alert views with titles, messages and buttons
//

#ifndef europeanasocial_UIAlertView_SimpleAlert_h
#define europeanasocial_UIAlertView_SimpleAlert_h

#import <UIKit/UIKit.h>

@interface UIAlertView (SimpleAlert)

+(void)simpleAlertWithMessage:(NSString*)message;
+(void)simpleAlertWithTitle:(NSString*)title andMessage:(NSString*)message;
+(void)simpleAlertWithTitle:(NSString*)title andMessage:(NSString*)message andButton:(NSString*)buttonText;

@end

#endif
//
//  Timeline.h
//  europeanasocial
//
//  Created by Terry Goodwin on 07/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Handles the placement of clips and moving around on the timeline
//

#ifndef europeanasocial_Timeline_h
#define europeanasocial_Timeline_h

#import <CoreMedia/CoreMedia.h>
#import <UIKit/UIKit.h>

@class Audio;
@class Clip;
@class MediaManager;
@class TimelineAudio;
@class TimelineVideo;

@interface Timeline : UIView {
    IBOutlet MediaManager* mediaManager;
    IBOutlet UIScrollView* scrollView;
    
    UIView* indicator;
    
    TimelineAudio* audio;
    
    int maxSlots;
    NSMutableDictionary* slots;
    
    bool draggingIndicator;
    
    bool initialized;
    
    NSDate* lastPreviewUpdateTime;
    
    Clip* blankClip;
    int32_t clipTimescales;
}

+(float)INCREMENT_LENGTH_SECONDS;

-(bool)droppedClip:(Clip*)clip withGestureRecognizer:(UILongPressGestureRecognizer*)gesture;
-(void)removeClip:(TimelineVideo*)clip;
-(void)initializeWithMusic:(Audio*)music;
-(void)moveIndicatorToTimeWithSeconds:(float)seconds;

-(NSArray*)clips;
-(CMTime)timeFromIndicator;

-(void)skipPreviewForwards;
-(void)skipPreviewBackwards;
-(void)rewind;
-(void)fastForward;

@end

#endif

//
//  Video.h
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of video, instantiated with a filename to create a VideoAsset object
//

#ifndef europeanasocial_Video_h
#define europeanasocial_Video_h

#import "Media.h"

@class VideoAsset;

@interface Video : Media {
    VideoAsset* asset;
}

@end

#endif

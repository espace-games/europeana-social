//
//  VideoAsset.m
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A VideoAsset derived from a MediaAsset, reports extension as mp4 and holds a thumbnail
//

#import "VideoAsset.h"

@implementation VideoAsset

-(NSString*)extension {
    return @"mp4";
}

-(id)initWithFilename:(NSString*)file {
    self = [super initWithFilename:file];
    if (self) {
        _thumbnail = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg", filename]];
    }
    
    return self;
}

-(UIImage*)thumbnail {
    return _thumbnail;
}

@end

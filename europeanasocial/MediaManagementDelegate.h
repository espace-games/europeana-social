//
//  MediaManagementDelegate.h
//  europeanasocial
//
//  Created by SGInt_003 on 12/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A delegate for the MediaManager, to call back when we get some muzick
//

#ifndef europeanasocial_MediaManagementDelegate_h
#define europeanasocial_MediaManagementDelegate_h

#import <Foundation/Foundation.h>

@class Audio;

@protocol MediaManagementDelegate <NSObject>

-(void)gotMusic:(Audio*)music;

@end

#endif
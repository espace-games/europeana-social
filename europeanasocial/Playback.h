//
//  Playback.h
//  europeanasocial
//
//  Created by SGInt_003 on 15/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Controller for the playback buttons
//

#ifndef europeanasocial_Playback_h
#define europeanasocial_Playback_h

#import <UIKit/UIKit.h>

@class MediaManager;
@class Timeline;

@interface Playback : UIView {
    IBOutlet UIButton* back;
    IBOutlet UIButton* rewind;
    IBOutlet UIButton* playPause;
    IBOutlet UIButton* fastForward;
    IBOutlet UIButton* forward;
    
    IBOutlet MediaManager* mediaManager;
    IBOutlet Timeline* timeline;
    
    UIImage* pauseImage;
    UIImage* playImage;
    
    bool doFF;
    bool doRW;
}

@end

#endif

//
//  Clip.m
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A high-level piece of media (a clip, if you will)
//

#import "Clip.h"

#import "Video.h"
#import "VideoAsset.h"

@implementation Clip

-(id)init {
    self = [super init];
    if (self) {
        _empty = true;
    }
    return self;
}

-(id)initWithFilename:(NSString*)file andVideo:(Video*)vid andIndex:(int)newIndex {
    self = [super init];
    if (self) {
        filename = file;
        _asset = [[VideoAsset alloc] initWithFilename:filename];

        if (_asset == nil) {
            return nil;
        }

        _index = newIndex;
        _video = vid;
        _title = [NSString stringWithFormat:@"Clip %d", newIndex];
        
        _empty = false;
    }
    
    return self;
}

-(UIImage*)thumbnail {
    return [_asset thumbnail];
}
-(VideoAsset*)asset {
    return _asset;
}
-(Video*)video {
    return _video;
}
-(NSString*)title {
    return _title;
}
-(int)index {
    return _index;
}

-(int)lengthSeconds {
    CMTime time = [_asset duration];
    return (int)(time.value/time.timescale);
}

@end

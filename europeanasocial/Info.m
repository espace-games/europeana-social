//
//  Info.m
//  europeanasocial
//
//  Created by SGInt_003 on 20/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Info box controller, handles fading in and out
//

#import "Info.h"

@implementation Info

-(IBAction)touchedInfo:(id)sender {
    if (!showingInfo) {
        [self showInfo];
    }
}
-(IBAction)touchedInfoClose:(id)sender {
    if (showingInfo) {
        [self hideInfo];
    }
}

-(void)showInfo {
    if (showingInfo) {
        return;
    }
    
    showingInfo = true;
    
    infoBox.alpha = 0.0;
    infoBox.hidden = false;
    infoBox.userInteractionEnabled = true;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25f];
    
    infoBox.alpha = 1.0f;
    
    [UIView commitAnimations];
}
-(void)hideInfo {
    if (!showingInfo) {
        return;
    }
    
    showingInfo = false;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25f];
    
    infoBox.alpha = 0.0f;
    
    [UIView commitAnimations];
    
    infoBox.userInteractionEnabled = false;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        infoBox.hidden = true;
    });
}

@end

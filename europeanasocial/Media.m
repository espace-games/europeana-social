//
//  Media.m
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of media - a Europeana data object coupled with a corresponding file
//

#import "Media.h"

#import "Europeana.h"

@implementation Media

-(id)initWithFilename:(NSString*)file andData:(EuropeanaObject*)europeanaData {
    self = [super init];
    if (self) {
        filename = file;
        _data = europeanaData;
    }
    
    return self;
}

-(EuropeanaObject*)data {
    return _data;
}

-(int)lengthSeconds {
    NSLog(@"Media lengthSeconds not implemented");
    return 0;
}

@end

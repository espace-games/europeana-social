//
//  TimelineClip.h
//  europeanasocial
//
//  Created by Terry Goodwin on 07/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of media on the timeline with a label
//

#ifndef europeanasocial_TimelineClip_h
#define europeanasocial_TimelineClip_h

#import <UIKit/UIKit.h>

@interface TimelineMedia : UIView {
    IBOutlet UILabel* label;
}

@end

#endif

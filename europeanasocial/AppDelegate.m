//
//  AppDelegate.m
//  europeanasocial
//
//  Created by Terry Goodwin on 06/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  The App Delegate, can return a reference to the MediaManager derived from the root view controller and handles some Facebook related functions
//

#import <FacebookSDK/FacebookSDK.h>

#import "AppDelegate.h"

#import "MainController.h"
#import "MediaManager.h"
#import "Timeline.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

-(void)applicationDidBecomeActive:(UIApplication *)application {
    [FBAppEvents activateApp];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    return YES;
}

-(MediaManager*)mediaManager {
    MainController* mainController = (MainController*)self.window.rootViewController;

    return [mainController mediaManager];
}

@end

//
//  ViewController.h
//  europeanasocial
//
//  Created by Terry Goodwin on 06/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  The main controller, initializes managers and handles operability between them
//

#ifndef europeanasocial_MainController_h
#define europeanasocial_MainController_h

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

#import "MediaManagementDelegate.h"

@class Clip;
@class MediaManager;
@class PlayerView;
@class ShareManager;
@class Timeline;
@class TimelineVideo;

@interface MainController : UIViewController <MediaManagementDelegate> {
    IBOutlet Timeline* timeline;
    IBOutlet MediaManager* _mediaManager;
    IBOutlet ShareManager* shareManager;
}

-(MediaManager*)mediaManager;

-(void)droppedClip:(Clip*)clip withGestureRecognizer:(UILongPressGestureRecognizer*)gesture;
-(void)removeClipFromTimeline:(TimelineVideo*)clip;

@end

#endif
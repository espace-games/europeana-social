//
//  UIView+EasyConstraints.h
//  europeanasocial
//
//  Created by Terry Goodwin on 07/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A convenience category to "easily" add constraints to a view programatically
//

#ifndef europeanasocial_UIView_EasyConstraints_h
#define europeanasocial_UIView_EasyConstraints_h

#import <UIKit/UIKit.h>

@interface UIView (EasyConstraints) {
}

-(void)constrainInView:(UIView*)parentView;
-(void)constrainInView:(UIView*)parentView withConstrantsRect:(CGRect)constraints;
-(void)removeConstraintsInView:(UIView*)parentView;

@end

#endif
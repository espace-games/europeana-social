//
//  Facebook.h
//  europeanasocial
//
//  Created by Terry Goodwin on 16/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Facebook sharing manager
//

#ifndef europeanasocial_Facebook_h
#define europeanasocial_Facebook_h

#import <FacebookSDK/FacebookSDK.h>

@class MediaManager;

typedef void (^FacebookShareCompletion)(bool);

@interface Facebook : NSObject {
    FBSession* readSession;
    FBSession* publishSession;
    FBSession* requestSession;
    
    bool sharing;
    
    MediaManager* mediaManager;
}

-(id)initWithMediaManager:(MediaManager*)newManager;

-(void)setMediaManager:(MediaManager*)newManager;

-(void)shareVideoWithCompletionHandler:(FacebookShareCompletion)completionHandler;
-(void)shareVideo;

@end

#endif

//
//  Video.m
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of video, instantiated with a filename to create a VideoAsset object
//

#import "Video.h"

#import "VideoAsset.h"

@implementation Video

-(id)initWithFilename:(NSString*)file andData:(EuropeanaObject*)europeanaData {
    self = [super initWithFilename:file andData:europeanaData];
    if (self) {
        asset = [[VideoAsset alloc] initWithFilename:filename];
        
        if (asset == nil) {
            return nil;
        }
    }
    
    return self;
}

-(int)lengthSeconds {
    CMTime time = [asset duration];
    return (int)(time.value/time.timescale);
}

@end

//
//  AudioAsset.h
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  An AudioAsset derived from a MediaAsset, only real differentiator is the extension is given as mp3
//

#ifndef europeanasocial_AudioAsset_h
#define europeanasocial_AudioAsset_h

#import "MediaAsset.h"

@interface AudioAsset : MediaAsset {
}

-(NSString*)extension;

@end

#endif

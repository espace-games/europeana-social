//
//  Info.h
//  europeanasocial
//
//  Created by SGInt_003 on 20/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Info box controller, handles fading in and out
//

#ifndef europeanasocial_Info_h
#define europeanasocial_Info_h

#import <UIKit/UIKit.h>

@interface Info : UIView {
    IBOutlet UIView* infoBox;
    
    bool showingInfo;
}

-(IBAction)touchedInfo:(id)sender;
-(IBAction)touchedInfoClose:(id)sender;

@end

#endif

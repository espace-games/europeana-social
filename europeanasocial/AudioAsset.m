//
//  AudioAsset.m
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  An AudioAsset derived from a MediaAsset, only real differentiator is the extension is given as mp3
//

#import "AudioAsset.h"

@implementation AudioAsset

-(NSString*)extension {
    return @"mp3";
}

@end

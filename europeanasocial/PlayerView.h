//
//  PlayerView.h
//  europeanasocial
//
//  Created by Terry Goodwin on 09/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A view containing the video preview
//

#ifndef europeanasocial_PlayerView_h
#define europeanasocial_PlayerView_h

#import <UIKit/UIKit.h>

@class AVPlayer;

@interface PlayerView : UIView {
    
}

@property (nonatomic) AVPlayer* player;

@end

#endif

//
//  VideoAsset.h
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A VideoAsset derived from a MediaAsset, reports extension as mp4 and holds a thumbnail
//

#ifndef europeanasocial_VideoAsset_h
#define europeanasocial_VideoAsset_h

#import <UIKit/UIKit.h>

#import "MediaAsset.h"

@interface VideoAsset : MediaAsset {
    UIImage* _thumbnail;
}

-(NSString*)extension;
-(UIImage*)thumbnail;

@end

#endif

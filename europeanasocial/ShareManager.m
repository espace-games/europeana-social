//
//  ShareManager.m
//  europeanasocial
//
//  Created by Terry Goodwin on 16/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Manager for the share functions and balloon
//

#import <AssetsLibrary/AssetsLibrary.h>

#import "ShareManager.h"

#import "Facebook.h"
#import "MediaManager.h"
#import "UIAlertView+SimpleAlert.h"

@implementation ShareManager

-(void)initialize {
    facebook = [[Facebook alloc] initWithMediaManager:mediaManager];
}

-(IBAction)touchedShare:(id)sender {
    [self toggleShareBalloon];
}

-(IBAction)touchedFacebook:(id)sender {
    [self showBusy];
    
    [self hideShareBalloon];
    [facebook shareVideoWithCompletionHandler:^(bool success) {
        [self hideBusy];
    }];
}
-(IBAction)touchedSave:(id)sender {
    [self hideShareBalloon];
    [self saveToCameraRoll];
}

-(void)toggleShareBalloon {
    if (showingBalloon) {
        [self hideShareBalloon];
        return;
    }
    [self showShareBalloon];
}

-(void)hideShareBalloon {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25f];
    
    shareBalloon.alpha = 0.0f;
    
    [UIView commitAnimations];

    shareBalloon.userInteractionEnabled = false;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        showingBalloon = false;
        shareBalloon.hidden = true;
    });
}
-(void)showShareBalloon {
    if (showingBalloon) {
        return;
    }
    
    showingBalloon = true;

    shareBalloon.alpha = 0.0;
    shareBalloon.hidden = false;
    shareBalloon.userInteractionEnabled = true;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25f];
    
    shareBalloon.alpha = 1.0f;
    
    [UIView commitAnimations];
}

-(void)saveToCameraRoll {
    [self showBusy];
    
    [mediaManager saveVideoWithCompletionHandler:^(NSURL* savedUrl){
        if (savedUrl == nil) {
            [self showBusy];
            return;
        }
        ALAssetsLibrary* assetsLibrary = [[ALAssetsLibrary alloc] init];
        if ([assetsLibrary videoAtPathIsCompatibleWithSavedPhotosAlbum:savedUrl]) {
            [assetsLibrary writeVideoAtPathToSavedPhotosAlbum:savedUrl completionBlock:^(NSURL* assetURL, NSError* error) {
                if(!error) {
                    [UIAlertView simpleAlertWithTitle:@"Video saved" andMessage:@"Your VidTime has been saved to your camera roll. Wow!"];
                } else {
                    [UIAlertView simpleAlertWithTitle:@"Oh dear" andMessage:error.localizedDescription];
                }
                [self hideBusy];
            }];
        }
    }];
    
}

-(void)hideBusy {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25f];
    
    busyView.alpha = 0.0f;
    
    [UIView commitAnimations];
    
    busyView.userInteractionEnabled = false;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        showingBusy = false;
        busyView.hidden = true;
    });
}
-(void)showBusy {
    if (showingBusy) {
        return;
    }
    
    showingBusy = true;
    
    busyView.alpha = 0.0;
    busyView.hidden = false;
    busyView.userInteractionEnabled = true;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25f];
    
    busyView.alpha = 1.0f;
    
    [UIView commitAnimations];
}



@end

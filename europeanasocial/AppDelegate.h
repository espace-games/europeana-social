//
//  AppDelegate.h
//  europeanasocial
//
//  Created by Terry Goodwin on 06/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  The App Delegate, can return a reference to the MediaManager derived from the root view controller and handles some Facebook related functions
//

#ifndef europeanasocial_AppDelegate_h
#define europeanasocial_AppDelegate_h

#import <UIKit/UIKit.h>

@class MediaManager;
@class Timeline;

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
}

@property (strong, nonatomic) UIWindow *window;

-(MediaManager*)mediaManager;

@end

#endif
//
//  ShareManager.h
//  europeanasocial
//
//  Created by Terry Goodwin on 16/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Manager for the share functions and balloon
//

#ifndef europeanasocial_ShareManager_h
#define europeanasocial_ShareManager_h

#import <UIKit/UIKit.h>

@class Facebook;
@class MediaManager;

@interface ShareManager : UIView {
    IBOutlet MediaManager* mediaManager;
    IBOutlet UIView* shareBalloon;
    IBOutlet UIView* busyView;
    
    bool showingBalloon;
    bool showingBusy;
    
    Facebook* facebook;
}

-(void)initialize;

-(IBAction)touchedShare:(id)sender;
-(IBAction)touchedFacebook:(id)sender;
-(IBAction)touchedSave:(id)sender;

@end

#endif

//
//  ViewController.m
//  europeanasocial
//
//  Created by Terry Goodwin on 06/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  The main controller, initializes managers and handles operability between them
//

#import "MainController.h"
#import "AppDelegate.h"

#import "Audio.h"
#import "Facebook.h"
#import "MediaManager.h"
#import "ShareManager.h"
#import "Timeline.h"
#import "TimelineVideo.h"
#import "UIView+EasyConstraints.h"

@interface MainController ()

@end

@implementation MainController

-(void)viewWillAppear:(BOOL)animated {
    [self setup];
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(MediaManager*)mediaManager {
    return _mediaManager;
}

-(void)setup {
    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (appDelegate == nil) {
        return;
    }

    [_mediaManager setDelegate:self];
    [_mediaManager initialize];
    
    [shareManager initialize];
}

-(void)droppedClip:(Clip*)clip withGestureRecognizer:(UILongPressGestureRecognizer*)gesture {
    if (![timeline droppedClip:clip withGestureRecognizer:gesture]) {
        return;
    }
    
    NSArray* clips = [timeline clips];
    [_mediaManager queueClips:clips];
}

-(void)gotMusic:(Audio*)music {
    [timeline initializeWithMusic:music];
}

-(void)removeClipFromTimeline:(TimelineVideo*)clip {
    [timeline removeClip:clip];
}

@end

//
//  ClipCell.m
//  europeanasocial
//
//  Created by Terry Goodwin on 09/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A clip cell, interact by tapping to preview or holding to move
//

#import "ClipCell.h"

#import "Clip.h"

@implementation ClipCell

-(void)setClip:(Clip*)newClip {
    _clip = newClip;

    label.text = [NSString stringWithFormat:@"Clip %d", [_clip index]];

    UIImage* clipThumbnail = [_clip thumbnail];
    [thumbnail setImage:clipThumbnail];
}

-(IBAction)touchedInfo:(id)sender {
    // Show info... Removed?
}

-(UIImage*)getRasterizedImageCopy {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0f);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(Clip*)clip {
    return _clip;
}

@end

//
//  MediaAsset.m
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A low-level audio-visual media asset
//

#import "MediaAsset.h"

@implementation MediaAsset

-(NSString*)extension {
    NSLog(@"MediaAsset extension - subclass and implement me!");
    return @"";
}

-(id)initWithFilename:(NSString*)file {
    self = [super init];
    if (self) {
        filename = file;
        
        NSString* path = [[NSBundle mainBundle] pathForResource:filename ofType:[self extension] inDirectory:nil];
        if (path == nil) {
            return nil;
        }
        
        url = [NSURL fileURLWithPath:path];
        _asset = [[AVURLAsset alloc] initWithURL:url options:nil];
        
        if (_asset == nil) {
            return nil;
        }
    }
    
    return self;
}

-(AVURLAsset*)asset {
    return _asset;
}

-(AVAssetTrack*)audioTrack {
    NSArray* tracks = [_asset tracks];
    for (AVAssetTrack* track in tracks) {
        if ([[track mediaType] compare:AVMediaTypeAudio] == NSOrderedSame) {
            return track;
        }
    }
    return nil;
}
-(AVAssetTrack*)videoTrack {
    NSArray* tracks = [_asset tracks];
    for (AVAssetTrack* track in tracks) {
        if ([[track mediaType] compare:AVMediaTypeVideo] == NSOrderedSame) {
            return track;
        }
    }
    return nil;
}

-(CMTime)duration {
    return [_asset duration];
}

@end

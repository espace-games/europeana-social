//
//  MediaManager.h
//  europeanasocial
//
//  Created by Terry Goodwin on 08/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  Manager for the media, handles creating video compositions/previews etc.
//

#ifndef europeanasocial_MediaManager_h
#define europeanasocial_MediaManager_h

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

#import "MediaManagementDelegate.h"

typedef void(^SavedVideoCallback)(NSURL*);

@class Audio;
@class Clip;
@class PlayerView;
@class Timeline;

@interface MediaManager : UIView {
    IBOutlet Timeline* timeline;
    IBOutlet UIViewController* _parentViewController;
    IBOutlet PlayerView* playerView;
    
    NSObject<MediaManagementDelegate>* _delegate;
    
    NSMutableArray* _assets;
    NSMutableArray* _videos;
    NSMutableArray* _clips;
    
    Audio* _music;

    AVPlayer* clipPlayer;
    AVPlayer* musicPlayer;
    AVPlayer* previewPlayer;
    
    NSMutableArray* previewAssets;
    AVMutableComposition* previewComposition;
    AVPlayerItem* previewItem;
    
    NSURL* previewUrl;

    AVPlayerItem* videoItem;
    AVPlayerItem* musicItem;
    AVPlayerItem* seekItem;
    
    Clip* _blankClip;
    Clip* currentClip;
    
    id playbackObserver;
    
    bool previewPaused;
}

-(void)initialize;

-(void)setDelegate:(NSObject<MediaManagementDelegate>*)newDelegate;
-(UIViewController*)parentViewController;

-(void)setPreviewOffset;
-(void)setPreviewOffsetWithHighPrecision;

-(Clip*)blankClip;

-(int)numClips;
-(NSArray*)clips;
-(Audio*)music;

-(void)playClip:(Clip*)clip;
-(void)queueClips:(NSArray*)clips;

-(bool)playing;
-(bool)playingClip;
-(bool)playingPreview;

-(void)playPreview;
-(void)pausePreview;
-(void)stop;

-(void)saveVideoWithCompletionHandler:(SavedVideoCallback)completionHandler;

@end

#endif

//
//  UIAlertView+SimpleAlert.m
//  europeanasocial
//
//  Created by Terry Goodwin on 16/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A convenience category to easily and cleanly show alert views with titles, messages and buttons
//

#import "UIAlertView+SimpleAlert.h"

@implementation UIAlertView (SimpleAlert)

+(void)simpleAlertWithMessage:(NSString*)message {
    [UIAlertView simpleAlertWithTitle:nil andMessage:message andButton:@"OK"];
}

+(void)simpleAlertWithTitle:(NSString*)title andMessage:(NSString*)message {
    [UIAlertView simpleAlertWithTitle:title andMessage:message andButton:@"OK"];
}

+(void)simpleAlertWithTitle:(NSString*)title andMessage:(NSString*)message andButton:(NSString*)buttonText {
    NSString* alertTitle = title;
    NSString* alertText = message;
    [[[UIAlertView alloc] initWithTitle:alertTitle
                                message:alertText
                               delegate:self
                      cancelButtonTitle:buttonText
                      otherButtonTitles:nil] show];
}

@end

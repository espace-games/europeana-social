//
//  TimelineVideo.h
//  europeanasocial
//
//  Created by Terry Goodwin on 07/01/2015.
//  Copyright (c) 2015 Serious Games International Ltd. All rights reserved.
//
//  A piece of video on the timeline, inherits a label from TimelineMedia
//

#ifndef europeanasocial_TimelineVideo_h
#define europeanasocial_TimelineVideo_h

#import "TimelineMedia.h"

@class Clip;

@interface TimelineVideo : TimelineMedia {
    IBOutlet UIImageView* thumbnail;
    IBOutlet UIButton* removeButton;
    
    Clip* _clip;
}

-(void)setClip:(Clip*)newClip;
-(Clip*)clip;

-(IBAction)removeClip:(id)sender;

@end

#endif
